/***************************************************************
 * Name:      main.cpp
 * Purpose:   Code for Application Class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-26
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include <QDir>
#include <QFileInfo>
#include <QApplication>

#include <ui/userwizard.h>
#include <helpers/database.h>

int main(int argc, char *argv[])
{
    //Check if database exists otherwise create it
    Database* Db = new Database();
    Db->checkDatabase();

    QApplication app(argc, argv);

    //Open a user wizard to login as an admin or customer
    UserWizard wizard;
    wizard.show();

    return app.exec();
}
