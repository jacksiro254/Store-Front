/***************************************************************
 * Name:      Customer.cpp
 * Purpose:   Defines the Customer class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QMetaType>

struct Customer {
    int id;
    QString name;
    QString mobile;
    QString address;
    QString created;
    QString updated;
};

Q_DECLARE_METATYPE(Customer)

#endif // CUSTOMER_H
