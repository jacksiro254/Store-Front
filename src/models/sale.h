/***************************************************************
 * Name:      Sale.cpp
 * Purpose:   Defines the Sale class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef SALE_H
#define SALE_H

#include <QMetaType>

struct Sale {
    int id;
    int productid;
    int customerid;
    int quantity;
    int amount;
    int tax;
    QString created;
    QString updated;
};

Q_DECLARE_METATYPE(Sale)

#endif // SALE_H
