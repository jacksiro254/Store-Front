/***************************************************************
 * Name:      Product.cpp
 * Purpose:   Defines the Product class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef PRODUCT_H
#define PRODUCT_H

#include <QMetaType>

struct Product {
    int id;
    QString title;
    QString description;
    int quantity;
    int price;
    int tax;
    QString created;
    QString updated;
};


Q_DECLARE_METATYPE(Product)

#endif // PRODUCT_H
