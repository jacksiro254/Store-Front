/***************************************************************
 * Name:      customerdelegate.cpp
 * Purpose:   Defines the CustomerDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef CUSTOMERDELEGATE_H
#define CUSTOMERDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class CustomerDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit CustomerDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~CustomerDelegate();
};

#endif // CUSTOMERDELEGATE_H
