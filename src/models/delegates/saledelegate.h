/***************************************************************
 * Name:      saledelegate.cpp
 * Purpose:   Defines the SaleDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef SALEDELEGATE_H
#define SALEDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class SaleDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit SaleDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~SaleDelegate();
};

#endif // SALEDELEGATE_H
