/***************************************************************
 * Name:      shopdelegate.cpp
 * Purpose:   Implements the ShopDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

#include "shopdelegate.h"
#include <models/product.h>

ShopDelegate::ShopDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

ShopDelegate::~ShopDelegate()
{

}

void ShopDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        Product item = var.value<Product>();

        // item Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width());
        rect.setHeight(option.rect.height());

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());

        QRectF productText1, productText2, productText3;

        productText1 = QRect(10, rect.top(), rect.width() - 150, 30);
        productText2 = QRect(10, productText1.bottom(), rect.width() - 150, 20);
        productText3 = QRect(productText1.right(), rect.top(), 100, 20);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText1, item.title);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 10, 0));
        painter->drawText(productText2, item.description);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText3, QString::number(item.price) + "/=");

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, productText2.bottom() + 5, rect.width(), productText2.bottom() + 5);

        painter->restore();
    }
}

QSize ShopDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
