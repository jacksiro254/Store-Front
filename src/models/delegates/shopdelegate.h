/***************************************************************
 * Name:      shopdelegate.h
 * Purpose:   Defines the ShopDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef SHOPDELEGATE_H
#define SHOPDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class ShopDelegate : public QStyledItemDelegate
{
    Q_OBJECT
signals:

public:
    explicit ShopDelegate(QObject* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const Q_DECL_OVERRIDE;

    ~ShopDelegate();
};

#endif // SHOPDELEGATE_H
