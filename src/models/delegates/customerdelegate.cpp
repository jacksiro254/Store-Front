/***************************************************************
 * Name:      customerdelegate.cpp
 * Purpose:   Implements the OrderDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include "customerdelegate.h"
#include "../storefront.h"

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

CustomerDelegate::CustomerDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

CustomerDelegate::~CustomerDelegate()
{

}

void CustomerDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        StoreFront item = var.value<StoreFront>();

        // item Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width());
        rect.setHeight(option.rect.height());

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());
        
        QRectF itemText0, itemText1, itemText2, itemText3, itemText4, itemText5;

        itemText0 = QRect(rect.left() + 5, rect.top(), 50, 50);
        itemText1 = QRect(itemText0.right(), rect.top(), rect.width() - 500, 40);
        itemText2 = QRect(itemText1.right(), rect.top(), 100, 30);
        itemText3 = QRect(itemText2.right(), rect.top(), 100, 30);
        itemText4 = QRect(itemText3.right(), rect.top(), 100, 30);
        itemText5 = QRect(itemText4.right(), rect.top(), 100, 40);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 15, 0));
        painter->drawText(itemText0, QString::number(item.id) + "#");

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(itemText1, item.text1);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 10, 0));
        painter->drawText(itemText2, item.text2);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(itemText3, item.text3);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(itemText4, item.text4);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 10, 0));
        painter->drawText(itemText5, item.text5);

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, rect.top() + 40, rect.width(), rect.top() + 40);

        painter->restore();
    }
}

QSize CustomerDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
