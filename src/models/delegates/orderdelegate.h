/***************************************************************
 * Name:      orderdelegate.cpp
 * Purpose:   Defines the OrderDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef ORDERDELEGATE_H
#define ORDERDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class OrderDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit OrderDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~OrderDelegate();
};

#endif // ORDERDELEGATE_H
