/***************************************************************
 * Name:      cataloguedelegate.cpp
 * Purpose:   Implements the CatalogueDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

#include "cataloguedelegate.h"
#include <models/product.h>

CatalogueDelegate::CatalogueDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

CatalogueDelegate::~CatalogueDelegate()
{

}

void CatalogueDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        Product item = var.value<Product>();

        // item Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width());
        rect.setHeight(option.rect.height());

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());

        QRectF productText1, productText2, productText3;

        productText1 = QRect(10, rect.top() + 5, 150, 50);
        productText2 = QRect(productText1.right(), rect.top() + 5, 75, 30);
        productText3 = QRect(productText2.right(), rect.top() + 5, 75, 30);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 14, 0));
        painter->drawText(productText1, item.title);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText2, QString::number(item.quantity));

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText3, QString::number(item.price) + "/=");

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, rect.top() + 30, rect.width(), rect.top() + 30);

        painter->restore();
    }
}

QSize CatalogueDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
