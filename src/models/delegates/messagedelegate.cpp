/***************************************************************
 * Name:      messagedelegate.cpp
 * Purpose:   Implements the MessageDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include "messagedelegate.h"
#include "../message.h"

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

MessageDelegate::MessageDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

MessageDelegate::~MessageDelegate()
{

}

void MessageDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        Message message = var.value<Message>();

        // message Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width() - 1);
        rect.setHeight(option.rect.height() - 10);

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());

        QRectF messageText1, messageText2, messageText3;

        messageText1 = QRect(rect.left() + 5, rect.top() + 2, rect.width() - 200, 20);
        messageText2 = QRect(rect.left() + 5, messageText1.bottom() - 5, rect.width() - 100, 20);
        messageText3 = QRect(messageText2.right(), messageText1.bottom(), 90, 20);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 10, QFont::Bold));
        painter->drawText(messageText1, message.title);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 8, 0));
        painter->drawText(messageText2, message.message);

        QStyleOptionProgressBar progressBar;
        progressBar.rect = option.rect.adjusted(messageText1.right(), 5, 0, -30);
        progressBar.minimum = 0;
        progressBar.maximum = 100;
        progressBar.progress = 10;

        progressBar.textVisible = true;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBar, painter);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 8, 0));
        painter->drawText(messageText3, message.created);

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, rect.top() + 40, rect.width(), rect.top() + 40);

        painter->restore();
    }
}

QSize MessageDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
