/***************************************************************
 * Name:      messagedelegate.cpp
 * Purpose:   Defines the MessageDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef MESSAGEDELEGATE_H
#define MESSAGEDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class MessageDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit MessageDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~MessageDelegate();
};

#endif // MESSAGEDELEGATE_H
