/***************************************************************
 * Name:      procustdelegate.cpp
 * Purpose:   Defines the ProductDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef PRODUCTDELEGATE_H
#define PRODUCTDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class ProductDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit ProductDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~ProductDelegate();
};

#endif // PRODUCTDELEGATE_H
