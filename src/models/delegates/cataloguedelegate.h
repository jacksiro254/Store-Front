/***************************************************************
 * Name:      cataloguedelegate.h
 * Purpose:   Defines the CatalogueDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef CATALOGUEDELEGATE_H
#define CATALOGUEDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class CatalogueDelegate : public QStyledItemDelegate
{
    Q_OBJECT
signals:

public:
    explicit CatalogueDelegate(QObject* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const Q_DECL_OVERRIDE;

    ~CatalogueDelegate();
};

#endif // CATALOGUEDELEGATE_H
