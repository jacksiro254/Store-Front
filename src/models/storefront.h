/***************************************************************
 * Name:      StoreFront.cpp
 * Purpose:   Defines the StoreFront class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef STOREFRONT_H
#define STOREFRONT_H

#include <QMetaType>

struct StoreFront {
    int id;
    QString text1;
    QString text2;
    QString text3;
    QString text4;
    QString text5;
    QString created;
    QString updated;
};

Q_DECLARE_METATYPE(StoreFront)

#endif // STOREFRONT_H
