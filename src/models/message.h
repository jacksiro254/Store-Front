/***************************************************************
 * Name:      Message.cpp
 * Purpose:   Defines the Message class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <QMetaType>

struct Message {
    int id;
    int status;
    QString title;
    QString message;
    QString created;
    QString updated;
};

Q_DECLARE_METATYPE(Message)

#endif // MESSAGE_H
