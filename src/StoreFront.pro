QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    helpers/database.cpp \
    main.cpp \
    models/delegates/cataloguedelegate.cpp \
    models/delegates/customerdelegate.cpp \
    models/delegates/messagedelegate.cpp \
    models/delegates/orderdelegate.cpp \
    models/delegates/productdelegate.cpp \
    models/delegates/saledelegate.cpp \
    models/delegates/shopdelegate.cpp \
    ui/dialogs/customerdialog.cpp \
    ui/dialogs/productdialog.cpp \
    ui/shopfront.cpp \
    ui/shopadmin.cpp \
    ui/userwizard.cpp

HEADERS += \
    exception.h \
    helpers/database.h \
    models/customer.h \
    models/delegates/cataloguedelegate.h \
    models/delegates/customerdelegate.h \
    models/delegates/messagedelegate.h \
    models/delegates/orderdelegate.h \
    models/delegates/productdelegate.h \
    models/delegates/saledelegate.h \
    models/delegates/shopdelegate.h \
    models/message.h \
    models/order.h \
    models/product.h \
    models/sale.h \
    models/storefront.h \
    ui/dialogs/customerdialog.h \
    ui/dialogs/productdialog.h \
    ui/shopfront.h \
    ui/shopadmin.h \
    ui/userwizard.h

FORMS += \
    ui/dialogs/customerdialog.ui \
    ui/dialogs/productdialog.ui \
    ui/shopfront.ui \
    ui/shopadmin.ui \
    ui/userwizard.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res/icons.qrc

DISTFILES +=
