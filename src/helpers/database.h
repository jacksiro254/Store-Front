#ifndef DATABASE_H
#define DATABASE_H

#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QStandardItemModel>

#include <models/customer.h>
#include <models/message.h>
#include <models/order.h>
#include <models/product.h>
#include <models/sale.h>
#include <models/storefront.h>

class Database : public QObject
{
    Q_OBJECT

public:
    QSqlDatabase appDB;
    QString timeNow, dbPath;
    void checkDatabase();
    void connectionClose(QString connectionName);
    void connectionOpen(QString connectionName);
    void initDbOperations();

    int addCustomer(Customer* customer);
    int addMessage(Message* message);
    int addOrder(Order* order);
    int addProduct(Product* product);
    int addSale(Sale* sale);

    QString sqlSafe(QString value);

    void latestCustomers(QStandardItemModel* model);
    void latestMessages(QStandardItemModel* model);
    void latestOrders(QStandardItemModel* model);
    void latestProducts(QStandardItemModel* model);
    void updateSoldProduct(int productid, int quantity);
    QVector< QVector<QString>> shopProducts(QStandardItemModel* model);
    void latestSales(QStandardItemModel* model);

    explicit Database();
    ~Database();

private:
    
};

#endif // DATABASE_H
