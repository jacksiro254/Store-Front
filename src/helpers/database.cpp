#include "database.h"

#include <QFile>
#include <QDebug>
#include <QDateTime>

Database::Database()
{
    timeNow = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    dbPath = "data/appdatabase.db";
}

// Add single quotes to values in a Query
QString Database::sqlSafe(QString value)
{
    return "'" + value + "'";
}

// Check if the database exists otherwise create it
void Database::checkDatabase()
{
    QFile file(dbPath);

    if (!file.exists())
    {
        QDir dir;
        if (!dir.exists("data")) dir.mkpath("data");

        file.open(QIODevice::ReadWrite);
        appDB = QSqlDatabase::addDatabase("QSQLITE");
        appDB.setDatabaseName(dbPath);

        if (!appDB.open()) {
            qDebug() << "database open failed.";
        }
        else 
        {
            initDbOperations();
            qDebug() << "database is okay.";
        }
    }
    else
    {
        appDB = QSqlDatabase::addDatabase("QSQLITE");
        appDB.setDatabaseName(dbPath);
        if (!appDB.open()) {
            qDebug() << "database open failed.";
        }
        else qDebug() << "database is okay.";
    }
    QSqlDatabase::removeDatabase(QSqlDatabase::defaultConnection);
    appDB.close();
}

// Open a connection to the database
void Database::connectionOpen(QString connectionName)
{
    appDB = QSqlDatabase::addDatabase("QSQLITE", connectionName);
    appDB.setDatabaseName(dbPath);
    if (!appDB.open()) {
        qDebug() << "database open failed.";
    }    
}

// Close a connection to the database
void Database::connectionClose(QString connectionName)
{
    QSqlDatabase::removeDatabase(connectionName);
    appDB.close();
}

// Do initial database operations
void Database::initDbOperations()
{
    //A vector with all the tables names we need in alphabetic order
    QVector<QString> tables;
    tables << "customers" << "messages" << "orders" << "products" << "sales";

    //Generic string for Table creation
    QString sqlCreate = "CREATE TABLE IF NOT EXISTS ";

    //Sql String for Creating Customers Table
    QString createCustomersTable = sqlCreate + 
        tables[0] + " (" +
            "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "name TEXT, " +
            "mobile	TEXT, " + 
            "address TEXT, " +
            "created TEXT, " +
            "updated TEXT" +
        ");";

    //Sql String for Creating Messages Table
    QString createMessagesTable = sqlCreate +
        tables[1] + " (" +
            "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "title TEXT, " +
            "message TEXT, " +
            "created TEXT, " +
            "updated TEXT" +
        ");";

    //Sql String for Creating Orders Table
    QString createOrdersTable = sqlCreate +
        tables[2] + " (" +
            "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "customerid INTEGER NOT NULL DEFAULT 0, " +
            "productid INTEGER NOT NULL DEFAULT 0, " +
            "quantity INTEGER NOT NULL DEFAULT 0, " +
            "amount INTEGER NOT NULL DEFAULT 0, " +
            "created TEXT, " +
            "updated TEXT" +
        ");";

    //Sql String for Creating Products Table
    QString createProductsTable = sqlCreate +
        tables[3] + " (" +
            "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "title TEXT, " +
            "description TEXT, " +
            "quantity INTEGER NOT NULL DEFAULT 0, " +
            "price INTEGER NOT NULL DEFAULT 0, " +
            "tax INTEGER NOT NULL DEFAULT 0, " +
            "created TEXT, " +
            "updated TEXT" +
        ");";

    //Sql String for Creating Sales Table
    QString createSalesTable = sqlCreate +
        tables[4] + " (" +
            "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "customerid INTEGER NOT NULL DEFAULT 0, " +
            "productid INTEGER NOT NULL DEFAULT 0, " +
            "quantity INTEGER NOT NULL DEFAULT 0, " +
            "amount INTEGER NOT NULL DEFAULT 0, " +
            "tax INTEGER NOT NULL DEFAULT 0, " +
            "created TEXT, " +
            "updated TEXT" +
        ");";

    //Creating Customers Table
    QSqlQuery qry0(appDB);
    if (!qry0.exec(createCustomersTable)) {
        qDebug() << qry0.lastError().text();
    }
    
    if (!appDB.tables().contains(tables[0])) {
        qDebug() << "Creating DB table: " + tables[0] +" failed.";
    }

    //Creating Messages Table
    QSqlQuery qry1(appDB);
    if (!qry1.exec(createMessagesTable)) {
        qDebug() << qry1.lastError().text();
    }

    if (!appDB.tables().contains(tables[1])) {
        qDebug() << "Creating DB table: " + tables[1] + " failed.";
    }

    //Creating Orders Table
    QSqlQuery qry2(appDB);
    if (!qry2.exec(createOrdersTable)) {
        qDebug() << qry2.lastError().text();
    }

    if (!appDB.tables().contains(tables[0])) {
        qDebug() << "Creating DB table: " + tables[2] + " failed.";
    }

    //Creating Products Table
    QSqlQuery qry3(appDB);
    if (!qry3.exec(createProductsTable)) {
        qDebug() << qry3.lastError().text();
    }

    if (!appDB.tables().contains(tables[0])) {
        qDebug() << "Creating DB table: " + tables[3] + " failed.";
    }

    //Creating Sales Table
    QSqlQuery qry4(appDB);
    if (!qry4.exec(createSalesTable)) {
        qDebug() << qry4.lastError().text();
    }

    if (!appDB.tables().contains(tables[0])) {
        qDebug() << "Creating DB table: " + tables[4] + " failed.";
    }

}

int Database::addCustomer(Customer* customer)
{
    //Sql String for Adding a Customer to the DB
    QString sqlInsert = "INSERT INTO customers ('name', 'mobile', 'address', 'created', 'updated')";
    sqlInsert.append("VALUES(" +
        sqlSafe(customer->name) + ", " +
        sqlSafe(customer->mobile) + ", " +
        sqlSafe(customer->address) + ", " +
        sqlSafe(timeNow) + ", " +
        sqlSafe(timeNow) +
        ");");

    //Executing the query
    QSqlQuery insertQry(appDB);
    if (!insertQry.exec(sqlInsert)) {
        qDebug() << sqlInsert + " failed.\n" + insertQry.lastError().text();
    }
    return insertQry.lastInsertId().toInt();
}

void Database::latestCustomers(QStandardItemModel* model)
{
    //Sql String for selecting Customers from the DB
    QString sqlSelect = "SELECT * FROM customers;";

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }

    while (selectQry.next()) {
        QStandardItem* qStdItem = new QStandardItem;
        StoreFront item;
        item.id = selectQry.value(0).toInt();
        item.text1 = selectQry.value(1).toString();
        item.text2 = selectQry.value(2).toString();
        item.text3 = selectQry.value(3).toString();

        qStdItem->setData(QVariant::fromValue(item), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
}

int Database::addMessage(Message* message)
{
    //Sql String for Adding a Message to the DB
    QString sqlInsert = "INSERT INTO messages ('title', 'message', 'created', 'updated')";
    sqlInsert.append("VALUES(" +
        sqlSafe(message->title) + ", " +
        sqlSafe(message->message) + ", " +
        sqlSafe(timeNow) + ", " +
        sqlSafe(timeNow) +
        ");");

    //Executing the query
    QSqlQuery insertQry(appDB);
    if (!insertQry.exec(sqlInsert)) {
        qDebug() << sqlInsert + " failed.\n" + insertQry.lastError().text();
    }
    return insertQry.lastInsertId().toInt();
}

void Database::latestMessages(QStandardItemModel* model)
{
    //Sql String for selecting Messages from the DB
    QString sqlSelect = "SELECT * FROM messages;";

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }

    while (selectQry.next()) {
        QStandardItem* qStdItem = new QStandardItem;
        Message message;
        message.id = selectQry.value(0).toInt();
        message.title = selectQry.value(1).toString();
        message.message = selectQry.value(2).toInt();
        message.created = selectQry.value(3).toString();
        message.updated = selectQry.value(4).toString();

        qStdItem->setData(QVariant::fromValue(message), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
}

int Database::addOrder(Order* order)
{
    //Sql String for Adding a Order to the DB
    QString sqlInsert = "INSERT INTO orders ('customerid', 'productid', 'quantity', 'amount', 'created', 'updated')";
    sqlInsert.append("VALUES(" +
        sqlSafe(QString::number(order->customerid)) + ", " +
        sqlSafe(QString::number(order->productid)) + ", " +
        sqlSafe(QString::number(order->quantity)) + ", " +
        sqlSafe(QString::number(order->amount)) + ", " +
        sqlSafe(timeNow) + ", " +
        sqlSafe(timeNow) +
        ");");

    //Executing the query
    QSqlQuery insertQry(appDB);
    if (!insertQry.exec(sqlInsert)) {
        qDebug() << sqlInsert + " failed.\n" + insertQry.lastError().text();
    }
    return insertQry.lastInsertId().toInt();
}

void Database::latestOrders(QStandardItemModel* model)
{
    //Sql String for selecting Orders from the DB
    QString sqlSelect = "SELECT orders.id, customers.name, products.title, orders.quantity, orders.amount ";
    sqlSelect.append("FROM orders ");
    sqlSelect.append("INNER JOIN customers ON orders.customerid = customers.id ");
    sqlSelect.append("INNER JOIN products ON orders.productid = products.id;");

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }

    while (selectQry.next()) {
        QStandardItem* qStdItem = new QStandardItem;
        StoreFront item;
        item.id = selectQry.value(0).toInt();
        item.text1 = selectQry.value(1).toString();
        item.text2 = selectQry.value(2).toString();
        item.text3 = selectQry.value(3).toString();
        item.text4 = selectQry.value(4).toString();

        qStdItem->setData(QVariant::fromValue(item), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
}

int Database::addProduct(Product* product)
{
    //Sql String for Adding a Product to the DB
    QString sqlInsert = "INSERT INTO products ('title', 'description', 'quantity', 'price', 'tax', 'created', 'updated')";
    sqlInsert.append("VALUES(" + 
        sqlSafe(product->title) + ", " +
        sqlSafe(product->description) + ", " +
        sqlSafe(QString::number(product->quantity)) + ", " +
        sqlSafe(QString::number(product->price)) + ", " +
        sqlSafe(QString::number(product->tax)) + ", " +
        sqlSafe(timeNow) + ", " +
        sqlSafe(timeNow) +
        ");");

    //Executing the query
    QSqlQuery insertQry(appDB);
    if (!insertQry.exec(sqlInsert)) {
        qDebug() << sqlInsert + " failed.\n" +insertQry.lastError().text();
    }
    return insertQry.lastInsertId().toInt();
}

QVector< QVector<QString>> Database::shopProducts(QStandardItemModel* model)
{
    QVector< QVector<QString>> products;

    //Sql String for selecting Products from the DB
    QString sqlSelect = "SELECT * FROM products WHERE quantity != 0;";

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }

    while (selectQry.next()) {        
        products.push_back({ 
            selectQry.value(0).toString(),
            selectQry.value(1).toString(),
            selectQry.value(2).toString(),
            selectQry.value(3).toString(),
            selectQry.value(4).toString(),
            selectQry.value(5).toString(),
            selectQry.value(6).toString(),
            selectQry.value(7).toString(),
        });

        QStandardItem* qStdItem = new QStandardItem;
        Product product;
        product.id = selectQry.value(0).toInt();
        product.title = selectQry.value(1).toString();
        product.description = selectQry.value(2).toString();
        product.quantity = selectQry.value(3).toInt();
        product.price = selectQry.value(4).toInt();
        product.tax = selectQry.value(5).toInt();
        product.created = selectQry.value(6).toString();
        product.updated = selectQry.value(7).toString();

        qStdItem->setData(QVariant::fromValue(product), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
    return products;
}

void Database::latestProducts(QStandardItemModel* model)
{
    //Sql String for selecting Products from the DB
    QString sqlSelect = "SELECT * FROM products;";

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }
    
    while (selectQry.next()) {
        QStandardItem* qStdItem = new QStandardItem;
        Product product;
        product.id = selectQry.value(0).toInt();
        product.title = selectQry.value(1).toString();
        product.description = selectQry.value(2).toString();
        product.quantity = selectQry.value(3).toInt();
        product.price = selectQry.value(4).toInt();
        product.tax = selectQry.value(5).toInt();
        product.created = selectQry.value(6).toString();
        product.updated = selectQry.value(7).toString();

        qStdItem->setData(QVariant::fromValue(product), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
}

void Database::updateSoldProduct(int productid, int quantity)
{
    //Sql String for Updating sold product
    QString sqlUpdate = "UPDATE products SET quantity = " + QString::number(quantity);
    sqlUpdate.append(" WHERE id=" + QString::number(productid) + ";");

    //Executing the query
    QSqlQuery updateQry(appDB);
    if (!updateQry.exec(sqlUpdate)) {
        qDebug() << sqlUpdate + " failed.\n" + updateQry.lastError().text();
    }
}

int Database::addSale(Sale* sale)
{
    //Sql String for Adding a Sale to the DB
    QString sqlInsert = "INSERT INTO sales ('customerid', 'productid', 'quantity', 'amount', 'tax', 'created', 'updated')";
    sqlInsert.append("VALUES(" +
        sqlSafe(QString::number(sale->customerid)) + ", " +
        sqlSafe(QString::number(sale->productid)) + ", " +
        sqlSafe(QString::number(sale->quantity)) + ", " +
        sqlSafe(QString::number(sale->amount)) + ", " +
        sqlSafe(QString::number(sale->tax)) + ", " +
        sqlSafe(timeNow) + ", " +
        sqlSafe(timeNow) +
        ");");

    //Executing the query
    QSqlQuery insertQry(appDB);
    if (!insertQry.exec(sqlInsert)) {
        qDebug() << sqlInsert + " failed.\n" + insertQry.lastError().text();
    }
    return insertQry.lastInsertId().toInt();
}

void Database::latestSales(QStandardItemModel* model)
{
    //Sql String for selecting Sales from the DB
    QString sqlSelect = "SELECT sales.id, customers.name, products.title, sales.quantity, sales.amount, sales.tax ";
    sqlSelect.append("FROM sales ");
    sqlSelect.append("INNER JOIN customers ON sales.customerid = customers.id ");
    sqlSelect.append("INNER JOIN products ON sales.productid = products.id;");

    //Executing the query
    QSqlQuery selectQry(appDB);
    if (!selectQry.exec(sqlSelect)) {
        qDebug() << sqlSelect + " failed.\n" + selectQry.lastError().text();
    }

    while (selectQry.next()) {
        QStandardItem* qStdItem = new QStandardItem;
        StoreFront item;
        item.id = selectQry.value(0).toInt();
        item.text1 = selectQry.value(1).toString();
        item.text2 = selectQry.value(2).toString();
        item.text3 = selectQry.value(3).toString();
        item.text4 = selectQry.value(4).toString();
        item.text5 = selectQry.value(5).toString();

        qStdItem->setData(QVariant::fromValue(item), Qt::UserRole + 1);
        model->appendRow(qStdItem);
    }
}

Database::~Database()
{

}