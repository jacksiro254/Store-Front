/***************************************************************
 * Name:      shopfront.h
 * Purpose:   Defines the Shop Front class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-26
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef SHOPFRONT_H
#define SHOPFRONT_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class ShopFront; }
QT_END_NAMESPACE

class ShopFront : public QMainWindow
{
    Q_OBJECT

    public:
        ShopFront(QWidget *parent = nullptr);
        void loadProducts();
        void on_BtnClear_clicked();
        QVector<QVector<int>> catalogues;
        ~ShopFront();

private slots:
        void on_actionExit_triggered();
        void closeEvent(QCloseEvent* event);

        void on_actionDelete_triggered();
        void on_actionRefresh_triggered();

        void on_LstRecords_clicked(const QModelIndex &index);

        void on_LblQty_valueChanged(int arg1);
        void on_BtnAdd_clicked();
        void on_BtnCheckout_clicked();
        void catalogueCreate();

private:
        Ui::ShopFront *ui;
};
#endif // SHOPFRONT_H
