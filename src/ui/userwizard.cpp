#include "userwizard.h"
#include "shopfront.h"
#include "shopadmin.h"
#include "ui_userwizard.h"

#include <QtWidgets>

UserWizard::UserWizard(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UserWizard)
{
    ui->setupUi(this);
    ui->GrpAdmin->hide();
    this->resize(500,300);
}

UserWizard::~UserWizard()
{
    delete ui;
}

void UserWizard::on_BtnAdminProceed_clicked()
{
    ui->GrpGuest->hide();
    ui->GrpAdmin->show();
}

void UserWizard::on_BtnGuestProceed_clicked()
{
    ShopFront* window = new ShopFront();
    window->showMaximized();
    this->close();
}

void UserWizard::on_BtnLogin_clicked()
{
    if (ui->TxtUsername->text().isEmpty()) {
        QMessageBox::warning(
            0,
            QObject::tr("Warning!"),
            QObject::tr("Please enter a valid username!")
        );
        return;
    }
    else if (ui->TxtPassword->text().isEmpty()) {
        QMessageBox::warning(
            0,
            QObject::tr("Warning!"),
            QObject::tr("Please enter a valid password!")
        );
        return;
    }
    else if (ui->TxtUsername->text() == QString::fromUtf8("admin")) {
        if (ui->TxtPassword->text() == QString::fromUtf8("1234")) {
            ShopAdmin* window = new ShopAdmin();
            //window->showMaximized();
            window->show();
            this->close();
        }
        else {
            QMessageBox::warning(
                0,
                QObject::tr("Warning!"),
                QObject::tr("The username and password you entered do not match our records!")
            );
            return;
        }
    }
    else {
        QMessageBox::warning(
            0,
            QObject::tr("Warning!"),
            QObject::tr("The username and password you entered do not match our records!")
        );
        return;
    }

}
