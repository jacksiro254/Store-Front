/***************************************************************
 * Name:      admin.cpp
 * Purpose:   Implements the Main Window class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-26
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include <QStandardItemModel>
#include <QObject>
#include <QCloseEvent>
#include <QtWidgets>
#include <QDialog>
#include <QFileInfo>

#include <models/product.h>
#include <models/sale.h>
#include <models/customer.h>
#include <models/order.h>
#include <models/message.h>


#include "shopfront.h"
#include "ui_shopfront.h"

#include "exception.h"
#include "userwizard.h"
#include <ui/dialogs/customerdialog.h>
#include <helpers/database.h>
#include <models/delegates/shopdelegate.h>
#include <models/delegates/cataloguedelegate.h>

CatalogueDelegate* catdelegate;
QStandardItemModel* shopModel;
QStandardItemModel* catModel;
QVector<QVector<QString>> products;
QString productTitle;
int productID, productQuantity, productPrice, productTax;

ShopFront::ShopFront(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ShopFront)
{
    ui->setupUi(this);
    loadProducts();
    catalogueCreate();
    ui->BtnClear->hide();
}

ShopFront::~ShopFront()
{
    delete ui;
}

void ShopFront::on_actionExit_triggered()
{
    this->close();
}

void ShopFront::closeEvent(QCloseEvent* event)
{
    UserWizard* wizard = new UserWizard();
    wizard->show();
    this->close();
}

void ShopFront::on_actionDelete_triggered()
{

}

void ShopFront::on_actionRefresh_triggered()
{
    loadProducts();
}

void ShopFront::loadProducts()
{
    Database* Db = new Database();
    shopModel = new QStandardItemModel();
    ShopDelegate* shopDelegate = new ShopDelegate(this);

    ui->LstRecords->setItemDelegate(shopDelegate);
    ui->LstRecords->setModel(shopModel);

    Db->connectionOpen("ShopProducts");
    products = Db->shopProducts(shopModel);
    Db->connectionClose("ShopProducts");

    QString countLabel = "Products (" + QString::number(shopModel->rowCount()) + ")";
    ui->LblPageName->setText(countLabel);
}

void ShopFront::on_LstRecords_clicked(const QModelIndex &index)
{
    int rowid = index.row();

    Product* product = new Product();
    productID = product->id = products[rowid][0].toInt();
    productTitle = product->title = products[rowid][1];
    product->description = products[rowid][2];
    productQuantity = product->quantity = products[rowid][3].toInt();
    productPrice = product->price = products[rowid][4].toInt();
    productTax = product->tax = products[rowid][5].toInt();
    
    //ui->TxtQty->setMaximum(productQuantity);
    ui->LblSelected->setText(product->title + " (" + QString::number(productQuantity) + " in Stock)");
        
}

void ShopFront::on_LblQty_valueChanged(int newQty)
{
    
}

void ShopFront::on_BtnAdd_clicked()
{
    catalogues.push_back({ 
        productID, 
        ui->TxtQty->value(), 
        ui->TxtQty->value() * productPrice,
        (productTax / 100) * productPrice,
        productQuantity - ui->TxtQty->value()
    });

    QStandardItem* qStdItem = new QStandardItem;
    Product catalogue;
    catalogue.id = productID;
    catalogue.title = productTitle;
    catalogue.price = ui->TxtQty->value() * productPrice;
    catalogue.quantity = ui->TxtQty->value();

    qStdItem->setData(QVariant::fromValue(catalogue), Qt::UserRole + 1);
    catModel->appendRow(qStdItem);
    ui->LblCatalogue->setText("Catalogue (" + QString::number(catModel->rowCount()) + ")");
}

void ShopFront::catalogueCreate()
{
    catModel = new QStandardItemModel();
    catdelegate = new CatalogueDelegate(this);

    ui->LstCalogue->setItemDelegate(catdelegate);
    ui->LstCalogue->setModel(catModel);
}

void ShopFront::on_BtnCheckout_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Are you done Shopping?");
    msgBox.setInformativeText("Do you want to finish shopping and checkout your Shopping Catalogue?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();

    switch (ret)
    {
        case QMessageBox::Ok:
            CustomerDialog dialog(this);
            dialog.exec();
            break;    
    }
}

void ShopFront::on_BtnClear_clicked()
{
    catalogues.clear();
    catalogueCreate();
    catModel->clear();
}
