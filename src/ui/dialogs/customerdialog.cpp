#include "customerdialog.h"
#include "ui_customerdialog.h"
#include <helpers/database.h>

CustomerDialog::CustomerDialog(ShopFront* parent) :
    QDialog(parent),
    ui(new Ui::CustomerDialog)
{
    shopFront = parent;
    ui->setupUi(this);
}

CustomerDialog::~CustomerDialog()
{
    delete ui;
}

void CustomerDialog::on_buttonBox_accepted()
{
    Customer* customer = new Customer();
    customer->name = ui->TxtName->text();
    customer->mobile = ui->TxtMobile->text();
    customer->address = ui->TxtAddress->text();

    addCustomer(customer);
}

void CustomerDialog::on_buttonBox_rejected()
{
    this->close();
}

void CustomerDialog::addCustomer(Customer* customer)
{
    Database* Db = new Database();
    Db->connectionOpen("NewCustomer");
    int newCustomerid = Db->addCustomer(customer);
    Db->connectionClose("NewCustomer");

    addOrderSales(newCustomerid);
}

void CustomerDialog::addOrderSales(int customerid)
{
    Database* Db = new Database();
    Db->connectionOpen("NewOrderSales");
    
    for (decltype(shopFront->catalogues.size()) i = 0; i < shopFront->catalogues.size(); ++i)
    {
        int productid = shopFront->catalogues[i][0];
        Order* order = new Order();
        order->customerid = customerid;
        order->productid = productid;
        order->quantity = shopFront->catalogues[i][1];
        order->amount = shopFront->catalogues[i][2];
        Db->addOrder(order);

        Sale* sales = new Sale();
        sales->productid = productid;
        sales->customerid = customerid;
        sales->quantity = shopFront->catalogues[i][1];
        sales->amount = shopFront->catalogues[i][2];
        sales->tax = shopFront->catalogues[i][3];
        Db->addSale(sales);

        Db->updateSoldProduct(productid, shopFront->catalogues[i][4]);
    }

    Db->connectionClose("NewOrderSales");
    this->close();
    shopFront->close();
    
}