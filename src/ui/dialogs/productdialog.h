#ifndef PRODUCTDIALOG_H
#define PRODUCTDIALOG_H

#include <QDialog>

#include <models/product.h>
#include <ui/shopadmin.h>

namespace Ui {
class ProductDialog;
}

class ProductDialog : public QDialog
{
    Q_OBJECT

public:
    Product* product;
    ShopAdmin* shopAdmin;
    explicit ProductDialog(ShopAdmin*parent = nullptr);
    ~ProductDialog();

private slots:
    void addProduct(Product* product);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_BtnUpload_clicked();

private:
    Ui::ProductDialog *ui;
};

#endif // PRODUCTDIALOG_H
