#ifndef CUSTOMERDIALOG_H
#define CUSTOMERDIALOG_H

#include <QDialog>

#include <models/customer.h>
#include <models/order.h>
#include <models/sale.h>
#include <ui/shopfront.h>

namespace Ui {
    class CustomerDialog;
}

class CustomerDialog : public QDialog
{
    Q_OBJECT

public:
    ShopFront* shopFront;
    explicit CustomerDialog(ShopFront* parent = nullptr);
    ~CustomerDialog();

private slots:
    void addCustomer(Customer* product);
    void addOrderSales(int customerid);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::CustomerDialog* ui;
};

#endif // PRODUCTDIALOG_H
CUSTOMERDIALOG_H