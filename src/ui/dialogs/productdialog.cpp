#include <QFileDialog>
#include <qDebug>

#include "productdialog.h"
#include "ui_productdialog.h"
#include <helpers/database.h>

QString fileName;

ProductDialog::ProductDialog(ShopAdmin*parent) :
    QDialog(parent),
    ui(new Ui::ProductDialog)
{
    shopAdmin = parent;
    ui->setupUi(this);
}

ProductDialog::~ProductDialog()
{
    delete ui;
}

void ProductDialog::on_buttonBox_accepted()
{
    if (ui->TxtTitle->text().isEmpty()) {
        ui->LblInstructions->setText("Please enter a valid Product Name!");
        return;
    }
    else if (ui->TxtQuantity->text().isEmpty()) {
        ui->LblInstructions->setText("Please enter a valid Product Quantity!");
        return;
    }
    else if (ui->TxtPrice->text().isEmpty()) {
        ui->LblInstructions->setText("Please enter a valid Product Price!");
        return;
    }
    else if (ui->TxtTax->text().isEmpty()) {
        ui->LblInstructions->setText("Please enter a valid Product Tax!");
        return;
    }
    else {
        Product* product = new Product();
        product->title = ui->TxtTitle->text();
        product->description = ui->TxtDescription->text();
        product->quantity = ui->TxtQuantity->value();
        product->price = ui->TxtPrice->value();
        product->tax = ui->TxtTax->value();

        addProduct(product);
    }
}

void ProductDialog::on_buttonBox_rejected()
{
    this->close();
}

void ProductDialog::addProduct(Product* product)
{
    Database* Db = new Database();
    Db->connectionOpen("NewProduct");
    Db->addProduct(product);
    Db->connectionClose("NewProduct");
    shopAdmin->loadProducts();
    this->close();
}

void ProductDialog::on_BtnUpload_clicked()
{
    //let upload a file!
    QFileDialog dia(this);
    QString defaultPath = QDir::currentPath();
    fileName = dia.getOpenFileName(this,
        tr("Upload a Text File into Inventory"), 
        defaultPath,
        tr("Text File (*.txt)", 0));
    dia.close();

    qDebug() << "File: " + fileName;

}
