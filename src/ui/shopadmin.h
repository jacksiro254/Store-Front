/***************************************************************
 * Name:      shopadmin.h
 * Purpose:   Defines the Main Window class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-26
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef SHOPADMIN_H
#define SHOPADMIN_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class ShopAdmin; }
QT_END_NAMESPACE

class ShopAdmin : public QMainWindow
{
    Q_OBJECT

    public:
        ShopAdmin(QWidget *parent = nullptr);
        void loadProducts();
        void loadSales();
        void loadCustomers();
        void loadOrders();
        void loadMessages();
        ~ShopAdmin();

private slots:
        void on_actionExit_triggered();
        void closeEvent(QCloseEvent* event);

        void on_actionAdd_triggered();
        void on_actionDelete_triggered();
        void on_actionRefresh_triggered();

        void on_actionSalesView_triggered();

        void on_actionCustomersView_triggered();

        void on_actionOrdersView_triggered();

        void on_actionMessagesView_triggered();

        void on_actionInventory_triggered();

        void on_actionInventoryView_triggered();

        void on_actionOrders_Per_Customer_triggered();

        void on_actionMost_Active_Customers_triggered();

        void on_actionMonthly_Sales_per_Item_triggered();

        void on_TxtSearch_textChanged(const QString &arg1);

private:
        Ui::ShopAdmin *ui;
};
#endif // SHOPADMIN_H
