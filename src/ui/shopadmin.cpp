/***************************************************************
 * Name:      shopadmin.cpp
 * Purpose:   Implements the Main Window class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-26
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include <QStandardItemModel>
#include <QObject>
#include <QCloseEvent>
#include <QtWidgets>
#include <QDialog>
#include <QFileInfo>

#include <models/product.h>
#include <models/sale.h>
#include <models/customer.h>
#include <models/order.h>
#include <models/message.h>
#include <models/storefront.h>

#include <models/delegates/productdelegate.h>
#include <models/delegates/saledelegate.h>
#include <models/delegates/customerdelegate.h>
#include <models/delegates/orderdelegate.h>
#include <models/delegates/messagedelegate.h>

#include "shopadmin.h"
#include "ui_shopadmin.h"

#include "userwizard.h"
#include <ui/dialogs/productdialog.h>
#include <helpers/database.h>

using namespace std;

int set_view = 0; // just to know what view we are in.

ShopAdmin::ShopAdmin(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ShopAdmin)
{
    ui->setupUi(this);
    ui->TxtSearch->hide();
    loadProducts();
}

ShopAdmin::~ShopAdmin()
{
    delete ui;
}

void ShopAdmin::on_actionExit_triggered()
{
    this->close();
}

void ShopAdmin::closeEvent(QCloseEvent* event)
{
    UserWizard* wizard = new UserWizard();
    wizard->show();
    this->close();
}

void ShopAdmin::on_actionAdd_triggered()
{
    ProductDialog dialog(this);
    dialog.exec();
}

void ShopAdmin::on_actionDelete_triggered()
{

}

void ShopAdmin::on_actionRefresh_triggered()
{
    switch (set_view)
    {
    case 0:
        loadProducts();
        break;
    case 1:
        loadSales();
        break;
    case 2:
        loadCustomers();
        break;
    case 3:
        loadOrders();
        break;
    case 4:
        loadMessages();
        break;
    }
}

void ShopAdmin::loadProducts()
{
    ui->actionAdd->setEnabled(true);
    ui->actionDelete->setEnabled(true);

    ui->LblText1->setText("PRODUCT");
    ui->LblText2->setText("");
    ui->LblText3->setText("QUANTITY");
    ui->LblText4->setText("PRICE");
    ui->LblText5->setText("UPDATED");

    Database* Db = new Database();
    QStandardItemModel* productModel = new QStandardItemModel();    
    ProductDelegate* productDelegate = new ProductDelegate(this);

    ui->LstRecords->setItemDelegate(productDelegate);
    ui->LstRecords->setModel(productModel);

    Db->connectionOpen("LatestProducts");
    Db->latestProducts(productModel);
    Db->connectionClose("LatestProducts");

    QString countLabel = "Products (" + QString::number(productModel->rowCount()) + ")";
    this->setWindowTitle("The Store Front - " + countLabel);
    ui->LblPageName->setText(countLabel);
}

void ShopAdmin::loadSales()
{
    ui->actionAdd->setEnabled(false);
    ui->actionDelete->setEnabled(false);

    ui->LblText1->setText("CUSTOMER");
    ui->LblText2->setText("PRODUCT");
    ui->LblText3->setText("QUANTITY");
    ui->LblText4->setText("AMOUNT");
    ui->LblText5->setText("TAX");

    Database* Db = new Database();
    QStandardItemModel* saleModel = new QStandardItemModel();
    SaleDelegate* saleDelegate = new SaleDelegate(this);

    ui->LstRecords->setItemDelegate(saleDelegate);
    ui->LstRecords->setModel(saleModel);

    Db->connectionOpen("LatestSales");
    Db->latestSales(saleModel);
    Db->connectionClose("LatestSales");

    QString countLabel = "Sales (" + QString::number(saleModel->rowCount()) + ")";
    this->setWindowTitle("The Store Front - " + countLabel);
    ui->LblPageName->setText(countLabel);
}

void ShopAdmin::loadCustomers()
{
    ui->actionAdd->setEnabled(false);
    ui->actionDelete->setEnabled(false);

    ui->LblText1->setText("NAME");
    ui->LblText2->setText("MOBILE");
    ui->LblText3->setText("ADDRESS");
    ui->LblText4->setText("");
    ui->LblText5->setText("");

    Database* Db = new Database();
    QStandardItemModel* customerModel = new QStandardItemModel();
    CustomerDelegate* customerDelegate = new CustomerDelegate(this);

    ui->LstRecords->setItemDelegate(customerDelegate);
    ui->LstRecords->setModel(customerModel);

    Db->connectionOpen("LatestCustomers");
    Db->latestCustomers(customerModel);
    Db->connectionClose("LatestCustomers");

    QString countLabel = "Customers (" + QString::number(customerModel->rowCount()) + ")";
    this->setWindowTitle("The Store Front - " + countLabel);
    ui->LblPageName->setText(countLabel);
}

void ShopAdmin::loadOrders()
{
    ui->actionAdd->setEnabled(false);
    ui->actionDelete->setEnabled(false);

    ui->LblText1->setText("CUSTOMER");
    ui->LblText2->setText("PRODUCT");
    ui->LblText3->setText("QUANTITY");
    ui->LblText4->setText("AMOUNT");
    ui->LblText5->setText("");

    Database* Db = new Database();
    QStandardItemModel* orderModel = new QStandardItemModel();
    OrderDelegate* orderDelegate = new OrderDelegate(this);

    ui->LstRecords->setItemDelegate(orderDelegate);
    ui->LstRecords->setModel(orderModel);

    Db->connectionOpen("LatestOrders");
    Db->latestOrders(orderModel);
    Db->connectionClose("LatestOrders");

    QString countLabel = "Orders (" + QString::number(orderModel->rowCount()) + ")";
    this->setWindowTitle("The Store Front - " + countLabel);
    ui->LblPageName->setText(countLabel);
}

void ShopAdmin::loadMessages()
{
    ui->actionAdd->setEnabled(false);
    ui->actionDelete->setEnabled(false);

    ui->LblText1->setText("PRODUCT");
    ui->LblText2->setText("DETAILS");
    ui->LblText3->setText("");
    ui->LblText4->setText("");
    ui->LblText5->setText("");

    Database* Db = new Database();
    QStandardItemModel* messageModel = new QStandardItemModel();
    MessageDelegate* messageDelegate = new MessageDelegate(this);

    ui->LstRecords->setItemDelegate(messageDelegate);
    ui->LstRecords->setModel(messageModel);

    Db->connectionOpen("LatestMessages");
    Db->latestMessages(messageModel);
    Db->connectionClose("LatestMessages");

    QString countLabel = "Messages (" + QString::number(messageModel->rowCount()) + ")";
    this->setWindowTitle("The Store Front - " + countLabel);
    ui->LblPageName->setText(countLabel);
}

void ShopAdmin::on_actionInventory_triggered()
{
    on_actionAdd_triggered();
}

void ShopAdmin::on_actionInventoryView_triggered()
{
    set_view = 0;
    loadProducts();
}

void ShopAdmin::on_actionSalesView_triggered()
{
    set_view = 1;
    loadSales();
}

void ShopAdmin::on_actionCustomersView_triggered()
{
    set_view = 2;
    loadCustomers();
}

void ShopAdmin::on_actionOrdersView_triggered()
{
    set_view = 3;
    loadOrders();
}

void ShopAdmin::on_actionMessagesView_triggered()
{
    set_view = 4;
    loadMessages();
}


void ShopAdmin::on_actionOrders_Per_Customer_triggered()
{
    set_view = 2;
    loadCustomers();
}

void ShopAdmin::on_actionMost_Active_Customers_triggered()
{
    set_view = 2;
    loadCustomers();
}

void ShopAdmin::on_actionMonthly_Sales_per_Item_triggered()
{
    set_view = 1;
    loadSales();
}

void ShopAdmin::on_TxtSearch_textChanged(const QString &arg1)
{

}
